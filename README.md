# MongooseHandler

This module handles a simple connection with Mongoose to a MongoDB server.

## Installation
<code>$ npm install mongoose-handler-nt --save</code>

## Usage

- <code>connect(config)</code>

Example of config

````json
  {
    "HOST": "127.0.0.1",
    "PORT": "27017",
    "DB_NAME": "DATABASE",
    "USERNAME": "USERNAME",
    "PASSWORD": "PASSWORD",
    "PARAMS": "?ssl=true"
  }
````
- <code>connectWithString(connectionString)</code>

The connectionString must be in the MongoDB format [https://docs.mongodb.com/manual/reference/connection-string/].

Then, you just need to insert mongoose in the files that will use it.

