const mongoose = require('mongoose');

const TagSchema = new mongoose.Schema(
{
  label: {type: String, default: null}
},
{
  collection : 'tags',
  strict: true
});

const Tag = mongoose.model('Tag', TagSchema);

module.exports = {Tag};

