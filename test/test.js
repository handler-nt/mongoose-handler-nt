const lib = require('../index.js');
const {Tag} = require('./Tag');

describe("Connect function", () =>
{
  /*
  it("Should not fail", (done) =>
  {
    lib.connectDB({DB_NAME: "test", HOST: "localhost"})
    .then(() =>
    {
      console.log('OK');
      done();
    })
    .catch(err =>
    {
      console.log(err);
      done();
    });
  });
  */
  it("Test connection with string", (done) =>
  {
    lib
    .connectWithString("")
    .then(() =>
    {
      console.log("OK");
      Tag
      .find()
      .then(data =>
      {
        console.log(data);
        done();
      })
      .catch(err =>
      {

      })
    })
    .catch(err =>
    {
      console.log(err);
      done();
    });
  })
});





