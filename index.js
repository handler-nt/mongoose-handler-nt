// BY NIKOSITECH
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

/**
 *
 * @param {Object} config : {USERNAME, PASSWORD, HOST (required), PORT, DB_NAME(required)}
 * @return {Promise<any>}
 */
function connect(config)
{
  return new Promise(async (resolve, reject) =>
  {
    if (config)
    {
      let connectionString = "mongodb://";
      if (config.USERNAME)
        connectionString += encodeURIComponent(config.USERNAME);
      if (config.PASSWORD)
        connectionString += ":"+encodeURIComponent(config.PASSWORD)+"@";
      if (config.HOST)
        connectionString += encodeURIComponent(config.HOST);
      if (config.PORT)
        connectionString += ":"+encodeURIComponent(config.PORT);
      if (config.DB_NAME)
        connectionString += "/"+encodeURIComponent(config.DB_NAME);
      if (config.PARAMS)
        connectionString += "?"+encodeURIComponent(config.PARAMS);

      mongoose.connect(connectionString,
      {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
      .then(() =>
      {
        resolve();
      })
      .catch(err =>
      {
        reject(err);
      });

    }
    else
      reject();
  });
}

function connectWithString(connectionString)
{
  return new Promise(async (resolve, reject) =>
  {
    mongoose.connect(connectionString,
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() =>
    {
      resolve();
    })
    .catch(err =>
    {
      reject(err);
    });
  });
}

function close()
{
  return new Promise((resolve, reject) =>
  {
    mongoose.connection.close(false, () =>
    {
      console.log("Mongoose connection closed.");
      return resolve(true);
    })
  });
}


module.exports =
{
  connectDB: connect,
  connect: connect,
  connectWithString,
  close
};
